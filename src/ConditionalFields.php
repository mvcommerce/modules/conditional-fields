<?php


namespace MVCommerceModules\ConditionalFields;


use Illuminate\Support\Arr;


/**
 * Class ConditionalFields
 * @package MVCommerceModules\ConditionalFields
 */
class ConditionalFields
{

    public function __construct()
    {
    }


    /**
     * @param array $conditions
     * @return array
     * @throws \Exception
     */
    public function groups($conditions = []){

        $groups = config('mvcommerce.conditional_fields.groups', []);

        $filtered_groups = [];

        $position = null;
        if(isset($conditions['position'])){
            $position = $conditions['position'];
            unset($conditions['position']);
        }

        foreach ($groups as $group_key => $group){

            $group = $this->mergeGroupDefaults($group, $group_key);

            // Filter for position, if position conditions are given.
            if( $position AND $group['position'] !== $position ) continue;

            // Filter by conditions.
            if(!$this->matchConditions($group['conditions'], $conditions)) continue;


            // If a match is found, then get the group's fields and
            // store in filtered groups.
            $group['resolved_key'] = $group_key;
            $group = $this->resolveFields($group);

            $filtered_groups[ $group_key ] = $group;
        }

        // Return groups sorted by the priority in Ascending order ----------------
        return collect($filtered_groups)->sortBy('priority')->toArray();
    }


    public function matchConditions(array $group_conditions, array $given_conditions){

        if( empty($group_conditions) OR empty( $given_conditions ) ){
            return true;
        }

        foreach ($group_conditions as $group_condition){

            $match_found = true;

            foreach ($group_condition as $key => $value){
                if( !isset($given_conditions[$key]) ){
                    break;
                }

                $c_group = Arr::wrap($value);
                $c_given = Arr::wrap($given_conditions[$key]);

                if( empty( array_intersect($c_given, $c_group) ) ){
                    $match_found = false;
                    break;
                }

            }

            if( $match_found ){
                return true;
            }
        }

        return false;

    }


    public function mergeGroupDefaults(array $group, string $group_key){

        $group['key']           = $group['key'] ?? $group_key;
        $group['position']      = $group['position'] ?? 'default';
        $group['priority']      = $group['priority'] ?? 10;
        $group['title']         = $group['title'] ?? ucwords($group_key);
        $group['conditions']    = $group['conditions'] ?? [];
        $group['fields']        = $group['fields'] ?? [];

        return $group;
    }

    public function resolveFields(array $group){
        $group['fields'] = $this->fields( $group['fields'], $group['resolved_key'] );
        return $group;
    }


    public function mergeFieldDefaults($field, $field_key){

        $field['key']           = $field['key'] ?? $field_key;
        $field['title']         = $field['title'] ?? ucwords($field_key);
        $field['type']          = $field['type'] ?? 'text';
        $field['attrs']         = $field['attrs'] ?? [];


        $global_defaults_all        = config('mvcommerce.conditional_fields.field_defaults.*', []);
        $global_defaults_of_type    = config("mvcommerce.conditional_fields.field_defaults.{$field['type']}", []);

        $field = array_replace_recursive([
            'attrs' => [
                'id' => $field_key,
                'name' => $field_key,
                'type' => $field['type'],
                'placeholder' => $field['title'],
            ]
        ], $field, $global_defaults_all, $global_defaults_of_type);

        return $field;

    }


    /**
     * @param string|array $field_keys
     * @param string $resolved_group_key
     * @return array
     */
    public function fields($field_keys, string $resolved_group_key){

        $fields = config('mvcommerce.conditional_fields.fields', []);

        $resolved_field_keys = [];
        foreach ($field_keys as $index => $value){

            if(is_scalar($value)){
                $resolved_field_keys[ $value ] = [];
            }else{
                $resolved_field_keys[ $index ] = (array) $value;
            }
        }

        $fields = array_intersect_key( $fields, $resolved_field_keys);

        foreach ($fields as $field_key => $field){

            $field = $this->mergeFieldDefaults($field, $field_key);
            $field = array_replace_recursive($field, $resolved_field_keys[$field_key]);

            if($field['type'] === 'repeater'){
                $field['resolved_key'] = "$resolved_group_key.$field_key.*";
                $field = $this->resolveFields($field);
            }

            $field['resolved_key'] = "$resolved_group_key.$field_key";
            $fields[ $field_key ] = $field;
        }

        return $fields;
    }

}
