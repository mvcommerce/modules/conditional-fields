<?php


namespace MVCommerceModules\ConditionalFields\Facades;


use Illuminate\Support\Facades\Facade;

/**
 * Class ConditionalFieldsFacade
 * @package MVCommerceModules\ConditionalFields\Facades
 *
 * @method static groups($conditions = []): array Get the groups.
 */
class ConditionalFieldsFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'mvcommerce.conditional_fields';
    }

}
