<?php


namespace MVCommerceModules\ConditionalFields\Providers;


use Illuminate\Support\ServiceProvider;
use MVCommerceModules\ConditionalFields\ConditionalFields;

class ConditionalFieldsServiceProvider extends ServiceProvider
{



    public function boot(){

        $this->publishes([
            __DIR__ . '/../../config/conditional_fields.php' => config_path('mvcommerce/conditional_fields.php'),
        ], 'config');

    }


    public function register(){

        $this->mergeConfigFrom(
            __DIR__ . '/../../config/conditional_fields.php', 'mvcommerce.conditional_fields'
        );

        $this->registerAbstracts();
        $this->loadHelpers();
    }


    public function registerAbstracts(){
        $this->app->singleton('mvcommerce.conditional_fields', function(){
            return new ConditionalFields();
        });
    }


    public function loadHelpers(){
        $path = __DIR__ . '/../../helpers/*.php';
        $files = glob($path);
        array_map(function($file){ require_once $file; }, $files);
    }


}
