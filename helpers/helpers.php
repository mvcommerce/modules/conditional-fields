<?php

if( !function_exists('conditional_fields') ){

    /**
     * @param null $conditions
     * @return \MVCommerceModules\ConditionalFields\ConditionalFields|array
     * @throws Exception
     */
    function conditional_fields($conditions = null){
        if(is_null($conditions)){
            return app('mvcommerce.conditional_fields');
        }else{
            return app('mvcommerce.conditional_fields')->groups($conditions);
        }
    }

}
