<?php

return [


    // Modify the default values for all fields.
    'field_defaults' => [
        '*'         => [ 'attrs' => [ 'class' => 'form-control' ] ],
        'checkbox'  => [ 'attrs' => [ 'class' => '' ] ],
        'radio'     => [ 'attrs' => [ 'class' => '' ] ],
    ],


    // Register the fields & it's options.

    'fields' => [

        'name' => [
            'key' => 'name',
            'type' => 'text',
            'title' => 'Name',
            'attrs' => [],
        ],

        'email' => [
            'key' => 'email',
            'type' => 'email',
            'title' => 'E-mail',
            'attrs' => [],
        ],

        'phone' => [
            'key' => 'phone',
            'title' => 'Phone',
            'type' => 'tel',
            'attrs' => [],
        ],

        'is_registered' => [
            'title' => 'Is Registered?',
            'type' => 'checkbox',
        ],

        'contacts' => [
            'key' => 'phone',
            'type' => 'repeater',
            'title' => 'Contacts',
            'fields' => ['email' => ['attrs' => ['name' => 'repeater_email']], 'phone']
        ],

    ],


    // Register field-groups. The fields must be an identifier/key of
    // previously registered field under 'fields' section.

    'groups' => [


        'contact' => [
            'key' => 'contact',
            'position' => 'sidebar',
            'priority' => 10,
            'title' => 'Contact',
            'conditions' => [
                [ 'post_type' => ['post', 'service'], 'taxonomy_term' => 'human' ],
                [ 'post_type' => 'user', 'taxonomy_term' => 'human' ],
            ],
            'fields' => ['name' => ['attrs' => ['name' => 'custom_name']], 'email', 'phone', 'is_registered']
        ],

        'users' => [

            'priority' => 5,

            'conditions' => [
                [ 'post_type' => ['user'] ],
            ],

            'fields' => ['name', 'contacts']
        ]

    ]

];
